import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { AlertsService } from "../alerts.service";
import { NotificationsService } from '../../shared/notifications.service';
import { DatePickerOptions } from '../../shared/date-picker-options';

import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  providers: [],
  styleUrls: ['./form.component.css']
})
export class FormComponent extends BaseForm implements OnInit {
  answers: any = {};
  datePickerOptions = DatePickerOptions;
  confirmPassword: any = "";
  countries: any;
  questanare: any;
  language: any;

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: AlertsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';
    this.getCountries();

    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      if(this.itemID)
        this.isEditable = true;

      this.getQuestanare();
    });
    if(this.itemID==null){
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/alerts`},{lable:'Add',url:`/alerts/new`}]);
    }else{
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/alerts`},{lable:'Edit',url:`/alerts/${this.itemID}/edit`}]);
    }
    
  }

  getItem() {
    this.item = {};
    this.busy = this.klassService.getItem(this.itemID, this.language).then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      this.item.password = "";

      this.item.questionary.forEach(_question => {
        this.answers[_question.questionary_id] = _question.questionary_text;
      });
    });
  }

  createItem() {

  }

  updateItem() {
  
  }

  getCountries () {
 
  }

  getQuestanare () {
  
  }
}
