import { Component, OnInit, EventEmitter } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Listing } from '../../shared/listing';

import { AlertsService } from '../alerts.service';
import { NotificationsService } from '../../shared/notifications.service';
import * as moment from 'moment';
@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent extends Listing implements OnInit {
  language: any;
  public sortBy   = "title";
  public sortOrder = "asc";
  constructor(
    private router: Router,
    private klassService: AlertsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';
    this.getItems();
    this.notificationsService.updateBreadCrumbs([{lable:'Alerts',url:`/alerts`}]);
  }

  getItems() {
    const filterParams = { "page": "1", "count": "100", "sort": "created_date",  "user_id": localStorage.getItem('userID')};
    this.busy = this.klassService.getNotifs(filterParams, this.language, 'admin/notification').then((res) => {
      if(!res)
        return true;
      console.log('notifications ... ', res)
      this.items = res.data;
      this.tmpItems = this.items;
      this.itemsTotal = this.items.length;
    });
  }

  readNotification(item){
    this.router.navigate([`/${item.url}`]);
    this.busy = this.klassService.updateItem(item._id, {_id:item._id}, this.language,  'admin/notification').then((res) => {
      if(!res)
        return true;
      this.router.navigate([`/${item.url}`]);
    });
  }
  formatDate(dateTime) {
    if (Math.abs(moment().diff(dateTime)) < 1000) {
      return 'just now';
    }
    return moment(dateTime).fromNow();
  }
  public filterItems() {
    if (this.tmpItems.length == 0)
      this.tmpItems = this.items;
    this.items = this.tmpItems.filter(_item =>
       _item.title.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1
    );
  }
  removeItem(){}
  toggleStatus(){}
}
