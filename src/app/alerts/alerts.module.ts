import { NgModule } from '@angular/core';

import { AppSharedModule } from '../shared/app-shared.module';
import { AlertsRoutingModule } from './alerts.routing';

import { AlertsComponent } from './alerts.component';
import { FormComponent } from './form/form.component';
import { ListingComponent } from './listing/listing.component';

import { AlertsService } from "./alerts.service";

@NgModule({
  imports: [
    AppSharedModule,
    AlertsRoutingModule
  ],
  providers: [AlertsService],
  declarations: [AlertsComponent, FormComponent, ListingComponent]
})
export class AlertsModule { }
