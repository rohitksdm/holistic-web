import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ThemeComponent } from './theme.component';

const routes: Routes = [
  {
    path: '',
    component: ThemeComponent,
    children: [
      {
        path: '',
        loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'dashboard',
        loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'clients',
        loadChildren: 'app/clients/clients.module#ClientsModule'
      },
      /*{
        path: 'staff',
        loadChildren: 'app/staff/staff.module#StaffModule'
      },*/
      /*{
        path: 'modules',
        loadChildren: 'app/modules/modules.module#ModulesModule'
      },
      {
        path: 'questions',
        loadChildren: 'app/questions/questions.module#QuestionsModule'
      },*/
      {
        path: 'alerts',
        loadChildren: 'app/alerts/alerts.module#AlertsModule'
      },
      {
        path: 'email-templates',
        loadChildren: 'app/email-templates/email-templates.module#EmailTemplatesModule'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ThemeRoutingModule { }
