import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";



@Component({
  selector: 'app-sidebar',
  providers: [],
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  language :any;
  modules: any = [];
  busy: Promise<any>;
  staticLinks: any = [];
  MiscellaneousDepartmentTypes = [];

  isAdmin: any = false;
  permissions: any;

  constructor(
  ) { }

  ngOnInit() {
    this.language = 'en';

    this.permissions = JSON.parse(localStorage.getItem('permissions'));

    this.isAdmin = localStorage.getItem('isAdmin') === 'true';
    if(this.isAdmin)
      this.getStaticLinks();

    this.MiscellaneousDepartmentTypes = ['service_request', 'maintenance_request', 'link', 'sharing', 'reservation'];
  }



  getStaticLinks() {
    this.staticLinks = [
      /*{ name: 'Users', link: '/clients', icon: 'fa-users' },
      { name: 'Health Coach', link: '/staff', icon: 'fa-users' },
      { name: 'Roles and Permissions', link: '/roles', icon: 'fa-key' },
      { name: 'Profile Questions', link: '/questions', icon: 'fa-question-circle' },
      { name: 'Modules', link: '/modules', icon: 'fa-sitemap' },*/
    ];
    // { name: 'Email Templates', link: '/email-templates', icon: 'fa-envelope' },
  }

}
