import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { ModuleWithProviders } from '@angular/core';

import { AuthGuard } from './services/auth-guard.service';
import { LoginComponent } from './auth/login/login.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';

export const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'forgot-password',
    component: ForgetPasswordComponent,
  },
  {
    path: 'forgot-password/reset/:token',
    component: ResetPasswordComponent,
  },
  {
    path: 'admin/forgot-password/reset/:admin_token',
    component: ResetPasswordComponent,
  },
  {
    path: '',
    loadChildren: 'app/theme/theme.module#ThemeModule',
    canActivate: [AuthGuard],
  },
  // {
  //   path: '**',
  //   redirectTo: 'login',
  //   pathMatch: 'full'
  // }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes, { useHash: true}),HttpModule ],
  exports: [ RouterModule,HttpModule ]
})
export class AppRoutingModule { }
