import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { EmailTemplatesService } from "../email-templates.service";
import { NotificationsService } from '../../shared/notifications.service';
import { DatePickerOptions } from '../../shared/date-picker-options';

import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  providers: [],
  styleUrls: ['./form.component.css']
})
export class FormComponent extends BaseForm implements OnInit {
    language: any;
    editor;
    public editorConfig = {
      placeholder: "",
      modules: {
        toolbar: [
          ['bold', 'italic', 'underline', 'strike'], 
          ['blockquote', 'code-block'],
          [{ 'header': 1 }, { 'header': 2 }], 
          [{ 'list': 'ordered'}, { 'list': 'bullet' }],
          [{ 'script': 'sub'}, { 'script': 'super' }],
          [{ 'indent': '-1'}, { 'indent': '+1' }],
          [{ 'direction': 'rtl' }],
          [{ 'size': ['small', false, 'large', 'huge'] }],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'color': [] }, { 'background': [] }],
          [{ 'font': [] }],
          [{ 'align': [] }],
          ['clean'],
          ['link']
        ]
      }
    };
  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: EmailTemplatesService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      if(this.itemID)
        this.isEditable = true;
        this.getItem();
    });
    if(this.itemID==null){
      this.notificationsService.updateBreadCrumbs([{lable:'Email Template',url:`/email-templates`},{lable:'Add',url:`/email-templates/new`}]);
    }else{
      this.notificationsService.updateBreadCrumbs([{lable:'Email Template',url:`/email-templates`},{lable:'Edit',url:`/email-templates/${this.itemID}/edit`}]);
    }
  }

  getItem() {
    this.item = {};
    this.busy = this.klassService.getItem(this.itemID, this.language, 'cms').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
    });
  }

  createItem() {
    
    this.busy = this.klassService.addItem(this.item, this.language, 'cms').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      this.router.navigate(['/email-templates']);
      this.notificationsService.notify('success', this.constantMessages.emailCreated);
    });
  }

  updateItem() {
    this.busy = this.klassService.updateItem(this.itemID, this.item, this.language, 'cms').then((res) => {
      if(!res)
        return true;
      this.notificationsService.notify('success', this.constantMessages.emailUpdated);
    });
  }

  onEditorBlured(quill) {
    console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
}
onContentChanged({ quill, html, text }) {
  console.log('quill content is changed!',   text);
  if(text.length>0)this.markDirty();
  // this.markDirty();
}
 

}
