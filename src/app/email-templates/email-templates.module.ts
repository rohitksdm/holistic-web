import { NgModule } from '@angular/core';
import { QuillEditorModule } from 'ngx-quill-editor';

import { AppSharedModule } from '../shared/app-shared.module';
import { EmailTemplatesRoutingModule } from './email-templates.routing';

import { EmailTemplatesComponent } from './email-templates.component';
import { FormComponent } from './form/form.component';
import { ListingComponent } from './listing/listing.component';

import { EmailTemplatesService } from "./email-templates.service";

@NgModule({
  imports: [
    AppSharedModule,
    EmailTemplatesRoutingModule,
    QuillEditorModule
  ],
  providers: [EmailTemplatesService],
  declarations: [EmailTemplatesComponent, FormComponent, ListingComponent]
})
export class EmailTemplatesModule { }
