import { Component, OnInit, EventEmitter } from '@angular/core';

import { Listing } from '../../shared/listing';

import { EmailTemplatesService } from '../email-templates.service';
import { NotificationsService } from '../../shared/notifications.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent extends Listing implements OnInit {
  language: any;
  constructor(
    private klassService: EmailTemplatesService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';
    this.getItems();
    this.notificationsService.updateBreadCrumbs([{ lable:'Email Templates', url:`/email-templates` }]);
  }

  getItems() {
    this.busy = this.klassService.getItems(this.language, 'cms').then((res) => {
      if(!res)
        return true;

      this.items = res.data;
      this.tmpItems = this.items;
      this.itemsTotal = this.items.length;
    });
  }

  removeItem() {

  }

  toggleStatus(_item) {
    let obj = { user_id: _item._id, is_active: !_item.is_active }
    this.busy = this.klassService.toggleStatus(obj, this.language).then((res) => {
      if(!res)
        return true;

      this.notificationsService.notify('success', this.constantMessages.emailStatus);
    });
  }
}
