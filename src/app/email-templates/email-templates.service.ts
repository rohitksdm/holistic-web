import { NgModule, Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';

import {BaseService} from "../services/base-service";
import { NotificationsService } from '../shared/notifications.service';

@Injectable()
export class EmailTemplatesService extends BaseService {
  constructor(
    private _router: Router,
    public http: Http,
    public notificationsService: NotificationsService) {
      super(http, notificationsService, 'user');
  }
}
