import { Component, OnInit } from '@angular/core';
import { MaterializeDirective } from "angular2-materialize";

import { DashboardService } from "./dashboard.service";
import { NotificationsService } from '../shared/notifications.service';


import * as moment from 'moment';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  items;
  busy;


  isAdmin: any = false;
  permissions: any;


  reservations_count: any;
  latest_reservations: any;
  users_count: any;
  modules:any;
  departments:any;
  durations: any = [
    { key: 7, value: "Last 7 days" },
    { key: 15, value: "Last 15 days" },
    { key: 30, value: "30 days" },
    { key: 182, value: "6 months" },
    { key: 364, value: "1 year" }
  ];

  duration: number = 7;
  language: any = 'en';

  STATUS: any={
    confirm:"Confirmed",
    cancel:"Canceled by User",
    pending: "Pending",
    suggestion_by_user: "Changed by User",
    suggestion_by_admin: "Changed by Staff"
  }

  permission:any;

  constructor(
    private klassService: DashboardService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit() {
    this.getItems();
    this.notificationsService.updateBreadCrumbs([
      { lable:'Email Templates', url:`/email-templates` }
    ]);

    this.permissions = JSON.parse(localStorage.getItem('permissions'));
    this.isAdmin = localStorage.getItem('isAdmin') === 'true';

    this.modules = JSON.parse(localStorage.getItem('modules'));
    this.departments = JSON.parse(localStorage.getItem('departments'));
    $("#breadcrumbs").html('<li><a href="/#/"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>');
}



  getItems() {
    this.items = [];
    this.busy = this.klassService.getItems(this.language, `viewDashboard/${this.duration}`).then((res) => {

      if(!res)
        return true;

      this.users_count = res.no_of_users;

      if(this.isAdmin) {
        this.reservations_count = res.number_of_reservation;
        this.latest_reservations = res.top_of_reservation;
      } else {
        this.reservations_count = res.number_of_reservation.filter(item => this.permissions[item._id] && this.permissions[item._id].manage_orders);
        this.latest_reservations = res.top_of_reservation.filter(item => this.permissions[item._id] && this.permissions[item._id].manage_orders);
      }


    });
  }

  formatDate(dateTime, time) {
    let date = dateTime.split('T')[0];
    if (time) {
      let time = dateTime.split('T')[1];
      return moment(date).format('MMM DD, YYYY') +' '+  moment(time, ["HH:mm:ss"]).format("hh:mm A");
    }else{
      return moment(date).format('MMM DD, YYYY')
    }
  }
}
