//orders
export class constMessages {

    constantMessages = {
        orderChange: 'Modification sent.',
        orderStatus: 'Status updated.',

        // clients
        userCreated: 'User created.',
        userUpdated: 'User updated.',
        userCheckedIn: 'User checked in.',
        userStatus: 'User status updated.',
        userDeleted: 'User deleted.',

        // departments
        departmentStatus: 'Department status updated.',
        departmentDeleted: 'Department deleted.',
        departmentCreated: 'Department created.',
        departmentUpdated: 'Department updated.',


        // modules
        moduleStatus: 'Module status updated.',
        moduleDeleted: 'Module deleted.',
        moduleCreated: 'Module created.',
        moduleUpdated: 'Module updated.',

        // products
        productStatus: 'Product status updated.',
        productDeleted: 'Product deleted.',
        productCreated: 'Product created.',
        productUpdated: 'Product updated.',


        // questions
        questionStatus: 'Question status updated.',
        questionDeleted: 'Question deleted.',
        questionCreated: 'Question created.',
        questionUpdated: 'Question updated.',


        // roles
        roleCreated: 'Role created.',
        roleUpdated: 'Role updated.',
        roleDeleted: 'Role deleted',


        // staff
        staffStatus: 'Staff status updated.',
        staffDeleted: 'Staff deleted.',
        staffCreated: 'Staff created.',
        staffUpdated: 'Staff updated.',

        // email
        emailStatus: 'Email status updated.',
        emailDeleted: 'Email deleted.',
        emailCreated: 'Email created.',
        emailUpdated: 'Email updated.',

    }
}