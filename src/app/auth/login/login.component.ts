import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, Route } from '@angular/router';
import { RouterModule } from '@angular/router';
import { LoginService } from './login.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  busy: Promise<any>;
  public user = { email: "", password: "" };
  public msg = '';
  public isForgotPassword: boolean = false;
  public emailNew: string;

  constructor(
    private _service: LoginService,
    private _router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    localStorage.removeItem('userID');
    localStorage.removeItem('token');
    localStorage.removeItem('email');
    localStorage.removeItem('name');
    localStorage.removeItem('mobile');
    localStorage.removeItem('permissions');
  }

  login() {
    this.busy = this._service.login(this.user).then(
      (res: any) => {
        let data = res.data;
        localStorage.setItem('userID', data._id);
        localStorage.setItem('isAdmin', data.is_admin);
        localStorage.setItem('token', res.token);
        localStorage.setItem('email', data.email);
        localStorage.setItem('name', data.name);
        localStorage.setItem('mobile', data.mobile_number);

        let permissions = {};
        data.permissions.forEach(permission => permissions[permission.module_id] =
                                               {
                                                 manage_orders: permission.manage_orders,
                                                 manage_service: permission.manage_service,
                                                 manage_staff: permission.manage_staff
                                               })

        localStorage.setItem('permissions', JSON.stringify(permissions));
        this._router.navigate(['/']);
      },
      (error) => {
        if(error.headers._headers.get('content-type')[0] == "application/json; charset=utf-8") {
          this.toastr.error(error.json().msg);
        } else {
          this.toastr.error('you are not able to login. Please try later.');
        }
      }
    );
  }

}
