import { Component, OnInit, EventEmitter } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { Listing } from '../../shared/listing';

import { ClientsService } from '../clients.service';
import { NotificationsService } from '../../shared/notifications.service';
import * as moment from 'moment'
@Component({
  selector: 'app-order-history',
  templateUrl: './order_history.component.html',
  styleUrls: ['./order_history.component.css']
})
export class OrderHistoryComponent extends Listing implements OnInit {
language: any;
itemID:any;
responseItems:any;
status = ['Select Status','Pending','Confirm','Cancel'];
selectedValue = 'Select Status';
modules:any;
booking_no:any="";
booking_detail:any="";
  constructor(
    private activatedRoute: ActivatedRoute,
    private klassService: ClientsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
   
    this.language = 'en'
    this.modules = JSON.parse(localStorage.getItem('modules'));
     this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      this.booking_no= params['booking'];
      if(this.itemID)
      this.getItems();
      this.getBooking();

    });
    this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Check-in',url:`/clients/${this.itemID}/checkin`},{lable:`#${this.booking_no}`,url:`/clients/${this.itemID}/orders/${this.booking_no}`}]);
    
  }

  // getItems() {
  //   const filterParams = { "page": "1", "count": "100", "sort": "created_date" };
  //   this.busy = this.klassService.getItems( this.language, `userorders/${this.itemID}`).then((res) => {
  //     if(!res)
  //       return true;
  //     console.log('res.data',res.data)
  //     this.items = res.data;
  //     this.tmpItems = this.items;
  //     this.responseItems= this.items;
  //     this.itemsTotal = this.items.length;
  //   });
  // }
  getItems() {
    this.items = [];

    this.busy = this.klassService.getItems(this.language, `userorders/${this.itemID}/${this.booking_no}`).then((res) => {
      if(!res)
        return true;

      console.log(res);
      this.items = res.data.filter( _item => _item.is_deleted != true && _item.customer_id!=null);
      this.tmpItems = this.items;
      this.responseItems= this.items;
      this.itemsTotal = this.items.length;

    });
  }
  getBooking() {
    this.busy = this.klassService.getItems(this.language, `usercheckinitem/${this.itemID}/${this.booking_no}`).then((res) => {
      if(!res)
        return true;

      this.booking_detail = res.data;
      //console.log('this.booking',res.data)
     
    });
  }

  removeItem() {
    
  }

  toggleStatus(_item) {
    
  }
   public filterItems() {
    if(this.tmpItems.length == 0)
      this.tmpItems = this.items;
    this.items = this.tmpItems.filter(_item => 
      (_item.confirmation_number && _item.confirmation_number.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1) ||
      (_item.department_id != undefined && _item.department_id.name.en.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1) ||
      (_item.module_id && this.modules[_item.module_id] != undefined && this.modules[_item.module_id].toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1)
    )
  }


  public searchEquipment(event) {
    // this.searchTerm = this.searchTerm.trimLeft();
    this.activePage = 1;
    this.filterItems();
  }
   changeStatus(status){
    if(status=="Select Status"){
      this.items = this.responseItems;
    }else{

      this.items = this.responseItems.filter( _item => _item.appointment_status.toLowerCase() == status.toLowerCase() );
    }

      this.tmpItems = this.items;
      this.itemsTotal = this.items.length;
  }
  formatDate(dateTime){
    let date = dateTime.split('T')[0];
    return moment(date).format('DD/MM/YYYY');
  }
}



