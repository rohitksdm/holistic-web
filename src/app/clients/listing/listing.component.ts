import { Component, OnInit, EventEmitter } from '@angular/core';

import { Listing } from '../../shared/listing';

import { ClientsService } from '../clients.service';
import { NotificationsService } from '../../shared/notifications.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent extends Listing implements OnInit {
language: any;
  constructor(
    private klassService: ClientsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.getItems();
    this.language = 'en'
    this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`}]);
  }

  getItems() {
    const filterParams = { "page": "1", "count": "100", "sort": "created_date" };
    this.busy = this.klassService.getXItems(filterParams, this.language, 'users').then((res) => {
      if(!res)
        return true;

      this.items = res.data;
      this.tmpItems = this.items;
      this.itemsTotal = this.items.length;
    });
  }

  removeItem() {
    this.closeModal();
    let obj = { user_id: this.item._id, is_deleted: true }
    this.busy = this.klassService.removeItem(obj, this.language).then((res) => {
      if(!res)
        return true;

      this.item = null;
      this.getItems();
      this.notificationsService.notify('success', this.constantMessages.userDeleted);
    });
  }

  toggleStatus(_item) {
    let obj = { user_id: _item._id, is_active: _item.is_active }
    this.busy = this.klassService.toggleStatus(obj, this.language).then((res) => {
      if(!res)
        return true;
      
      this.notificationsService.notify('success', this.constantMessages.userStatus);
    });
  }
  public filterItems() {
    if (this.tmpItems.length == 0)
      this.tmpItems = this.items;
    this.items = this.tmpItems.filter(_item =>
       _item.name.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1 || 
       _item.email.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1 ||
       _item.mobile_number.toLowerCase().lastIndexOf(this.searchTerm.trim().toLowerCase()) != -1
    );
  }
}
