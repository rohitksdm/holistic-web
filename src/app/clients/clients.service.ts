import { NgModule, Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';

import {BaseService} from "../services/base-service";
import { NotificationsService } from '../shared/notifications.service';

@Injectable()
export class ClientsService extends BaseService {
  constructor(
    private _router: Router,
    public http: Http,
    public notificationsService: NotificationsService) {
      super(http, notificationsService, 'user');
  }

  updateItem(id, obj, language) {
    const headers = new Headers({ 'user_id': id, language: language });
    const options = new RequestOptions({ headers: headers });
    return this.http.put(this.url.get('api') + 'account/', obj, options)
                    .toPromise().then(response => response.json())
                    .catch(data => this.handleError(data));
  }
  userCheckin(id, obj, language) {
    const headers = new Headers({ 'user_id': id, language: language });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(this.url.get('api') + 'checkin', obj, options)
                    .toPromise().then(response => response.json())
                    .catch(data => this.handleError(data));
  }
}
