import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { ClientsService } from "../clients.service";
import { NotificationsService } from '../../shared/notifications.service';
import { DatePickerOptions } from '../../shared/date-picker-options';

import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  providers: [],
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent extends BaseForm implements OnInit {
  language: any;
 
  public searchTerm            = '';
  public rowsOnPage            = 5;
  public activePage            = 1;
  public itemsTotal            = null;
  public rowsInTable           = [5,10,25];
  public sortBy                = "name";
  public sortOrder             = "asc";

 //item: any = null;
  items: any = [];
  tmpItems: any = [];
  responseItems = [];
  modules: any = [];
  booking_no:any="";
  booking_detail:any;
  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: ClientsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.language = 'en';

    this.modules = JSON.parse(localStorage.getItem('modules'));
    console.log("modules", this.modules);
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      this.booking_no= params['booking'];
      if(this.itemID)
        this.isEditable = true;

      
      this.getItems();
      this.getItem();
      this.getBooking();
    });
   
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Check-in',url:`/clients/${this.itemID}/checkin`},{lable:`#${this.booking_no}`,url:`/clients/${this.itemID}/invoice/${this.booking_no}`}]);
    

    
  }
   print(): void { 
       let printContents, popupWin;
       printContents = document.getElementsByClassName('sb2-2-add-blog')[0].innerHTML;
       popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
       popupWin.document.open();
      // alert(printContents)
       popupWin.document.write(`
          <html>
              <head>
                  <title>Print tab</title>
                  <style>
                      //........Customized style.......
                  </style>
              </head>
              <body onload="window.print();window.close()">${printContents}
              </body>
          </html>`
       );
       popupWin.document.close();
    }
    charPrint(no:number){
      var chr = String.fromCharCode(97 + no);
      return chr;
    }

  getItem() {
    this.busy = this.klassService.getItem(this.itemID, this.language, 'getuser').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      console.log('this.item',this.item)
     
    });
  }
  getBooking() {
    this.busy = this.klassService.getItems(this.language, `usercheckinitem/${this.itemID}/${this.booking_no}`).then((res) => {
      if(!res)
        return true;

      this.booking_detail = res.data;
      //console.log('this.booking',res.data)
     
    });
  }

  createItem() {
    
  }

  updateItem() {
   
  }

  

 

  

    getItems() {
    this.items = [];

    this.busy = this.klassService.getItems(this.language, `userorders/${this.itemID}/${this.booking_no}`).then((res) => {
      if(!res)
        return true;

      console.log(res);
      this.items = res.data.filter( _item => _item.is_deleted != true && _item.customer_id!=null && _item.appointment_status=='confirm');
      this.tmpItems = this.items;
      this.responseItems= this.items;
      this.itemsTotal = this.items.length;

    });
  }

   

  calculateSum(obj:any){
    let sum=0;
    if(obj.length>0){
      for (let i = 0; i < obj.length; i++) {
        sum+=obj[i].item_price*((obj[i].item_quantity)?obj[i].item_quantity:1);
      }

    }
    return sum;

  }
  calculateTotal(obj:any){
    let sum=0;
    for (let j = 0; j < obj.length; j++) {
    if(obj[j].order_items.length>0){
      for (let i = 0; i < obj[j].order_items.length; i++) {
        sum+=obj[j].order_items[i].item_price*((obj[j].order_items[i].item_quantity)?obj[j].order_items[i].item_quantity:1);
      }

    }
  }
    return sum;

  }


}
