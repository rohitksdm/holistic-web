import { NgModule } from '@angular/core';

import { AppSharedModule } from '../shared/app-shared.module';
import { ClientsRoutingModule } from './clients.routing';

import { ClientsComponent } from './clients.component';
import { FormComponent } from './form/form.component';
import { ListingComponent } from './listing/listing.component';
import { HistoryComponent } from './history/history.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { OrderHistoryComponent } from './order_history/order_history.component';

import { ClientsService } from "./clients.service";

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
//import {NgxMaskModule} from 'ngx-mask'
@NgModule({
  imports: [
    AppSharedModule,
    ClientsRoutingModule,
    BsDropdownModule.forRoot(),
    NgxIntlTelInputModule,
    //NgxMaskModule.forRoot()
  ],
  providers: [ClientsService],
  declarations: [ClientsComponent, FormComponent, ListingComponent,HistoryComponent,InvoiceComponent,OrderHistoryComponent]
})
export class ClientsModule { }
