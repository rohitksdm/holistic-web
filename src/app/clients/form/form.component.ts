import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { ClientsService } from "../clients.service";
import { NotificationsService } from '../../shared/notifications.service';
import { DatePickerOptions } from '../../shared/date-picker-options';

import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  providers: [],
  styleUrls: ['./form.component.css']
})
export class FormComponent extends BaseForm implements OnInit {
  answers: any = {};
  datePickerOptions = DatePickerOptions;
  confirmPassword: any = "";
  countries: any;
  questanare: any;
  language: any;

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: ClientsService,
    private notificationsService: NotificationsService
  ) { super(); }

  ngOnInit() {
    this.getCountries();
    this.language = 'en';
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      if(this.itemID)
        this.isEditable = true;

      this.getQuestanare();
    });
    if(this.itemID==null){
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Add',url:`/clients/new`}]);
    }else{
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Edit',url:`/clients/${this.itemID}/edit`}]);
    }
    
  }

  getItem() {
    this.busy = this.klassService.getItem(this.itemID, this.language, 'getuser').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      this.item.password = "";

      this.item.questionary.forEach(_question => {
        this.answers[_question.questionary_id] = _question.questionary_text;
      });
    });
  }

  createItem() {
    this.item.questionary = [];
    for (let key in this.answers) {
      this.item.questionary.push({questionary_id: key, questionary_text: this.answers[key]});
    }
    this.busy = this.klassService.addItem(this.item, this.language, 'signupweb').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      this.router.navigate(['/clients']);
      this.notificationsService.notify('success', this.constantMessages.userCreated);
    });
  }

  updateItem() {
    if(!this.item || this.item.password.length <= 0){
      delete this.item.password
    }
    this.busy = this.klassService.updateItem(this.itemID, this.item, this.language).then((res) => {
      if(!res)
        return true;

      this.item.password = "";
      this.form.form.markAsPristine();
      this.notificationsService.notify('success', this.constantMessages.userUpdated);
    });
  }

  getCountries () {
    this.busy = this.klassService.getCountries(this.language).then((res) => {
      if(!res)
        return true;

      this.countries = res.data;
    });
  }

  getQuestanare () {
    
  }
}
