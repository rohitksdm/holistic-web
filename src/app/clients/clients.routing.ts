import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientsComponent } from './clients.component';
import { FormComponent } from './form/form.component';
import { ListingComponent } from './listing/listing.component';
import { HistoryComponent } from './history/history.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { OrderHistoryComponent } from './order_history/order_history.component';


const routes: Routes = [
  {
    path: '',
    component: ClientsComponent,
    data: { title: 'Users' },
    children: [
      { path: '', component: ListingComponent, data: { title: '' } },
      { path: 'new', component: FormComponent, data: { title: 'New' } },
      { path: ':id/edit', component: FormComponent, data: { title: 'Edit' } },
      { path: ':id/checkin', component: HistoryComponent, data: { title: 'Checkin' } },
      { path: ':id/invoice/:booking', component: InvoiceComponent, data: { title: 'Invoice' } },
      { path: ':id/orders/:booking', component: OrderHistoryComponent, data: { title: 'Order History' } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ClientsRoutingModule { }
