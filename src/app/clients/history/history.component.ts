import { Component, OnInit,EventEmitter } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';

import { ClientsService } from "../clients.service";
import { NotificationsService } from '../../shared/notifications.service';
import { DatePickerOptions } from '../../shared/date-picker-options';

import { BaseForm } from '../../shared/base-form';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent extends BaseForm implements OnInit {
 
  language: any;
  
  public searchTerm            = '';
  public rowsOnPage            = 5;
  public activePage            = 1;
  public itemsTotal            = null;
  public rowsInTable           = [5,10,25];
  public sortBy                = "name";
  public sortOrder             = "asc";

 //item: any = null;
  items: any = [];
  tmpItems: any = [];
   responseItems=[];
    modules: any = [];
    invoiceItems:any=[];
    modalActions = new EventEmitter<string|MaterializeAction>();
    checkin_detail:any={};
  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private klassService: ClientsService,  
    private notificationsService: NotificationsService
  ) { super(); }


  openModal(_item) {
    this.item = _item;
    this.modalActions.emit({action:"modal",params:['open']});
  }

  closeModal() {
    this.modalActions.emit({action:"modal",params:['close']});
  }

  ngOnInit() {
   
    this.language = 'en';
    this.modules = JSON.parse(localStorage.getItem('modules'));
    // subscribe to router event
    this.activatedRoute.params.subscribe((params: Params) => {
      this.itemID = params['id'];
      if(this.itemID)
        this.isEditable = true;

      this.getItems();
      this.getItem();
    });
    if(this.itemID==null){
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Add',url:`/clients/new`}]);
    }else{
      this.notificationsService.updateBreadCrumbs([{lable:'Users',url:`/clients`},{lable:'Check-In',url:`/clients/${this.itemID}/checkin`}]);
    }
    
  }

  getItem() {
   this.busy = this.klassService.getItem(this.itemID, this.language, 'getuser').then((res) => {
      if(!res)
        return true;

      this.item = res.data;
      
     
    });
  }

  createItem() {
    
  }

  updateItem() {
   
  }

    getItems() {
    this.items = [];

    this.busy = this.klassService.getItems(this.language, `usercheckin/${this.itemID}`).then((res) => {
      if(!res)
        return true;

     this.invoiceItems = res.data.filter( _item => _item.is_deleted != true && _item.customer_id!=null && _item.appointment_status=='confirm');
      this.items = res.data;
      this.tmpItems = this.items;
      this.responseItems= this.items;
      this.itemsTotal = this.items.length;
       console.log('this.tmpItems',this.tmpItems);

    });
  }

  manualCheckin(){

    console.log("checkin_detail",this.checkin_detail)
    
    this.closeModal();    
   
    this.busy = this.klassService.userCheckin(this.itemID ,this.checkin_detail ,this.language).then((res) => {
      if(!res)
        return true;      
      this.notificationsService.notify('success', this.constantMessages.userCheckedIn);
      this.getItems();
    });
  }

  
 


}
